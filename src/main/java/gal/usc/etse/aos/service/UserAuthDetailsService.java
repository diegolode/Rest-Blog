package gal.usc.etse.aos.service;

import gal.usc.etse.aos.model.FullUser;
import gal.usc.etse.aos.repository.FullUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserAuthDetailsService implements UserDetailsService {
    private FullUserRepository db;

    @Autowired
    UserAuthDetailsService(FullUserRepository db){
        this.db = db;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<FullUser> user = db.findById(username);

        if(!user.isPresent())
            throw new UsernameNotFoundException(username);

        String roles = user.get().getRole();

        List<GrantedAuthority> authorities = AuthorityUtils.commaSeparatedStringToAuthorityList(roles);

        return new User(user.get().getUsername(), user.get().getPassword(), user.get().getEnabled(), true, true, true, authorities);
    }
}
