package gal.usc.etse.aos.repository;

import gal.usc.etse.aos.model.FullUser;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.awt.print.Pageable;

public interface FullUserRepository extends MongoRepository<FullUser, String> {
    Page<FullUser> findByRole(String role, PageRequest pageRequest);
}
