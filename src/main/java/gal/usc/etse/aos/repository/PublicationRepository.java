package gal.usc.etse.aos.repository;

import gal.usc.etse.aos.model.FullUser;
import gal.usc.etse.aos.model.Publication;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface PublicationRepository extends MongoRepository<Publication, String> {
    Page<Publication> findByAuthor(String author, PageRequest pageRequest);
}
