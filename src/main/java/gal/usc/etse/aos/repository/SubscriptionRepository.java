package gal.usc.etse.aos.repository;

import gal.usc.etse.aos.model.Comment;
import gal.usc.etse.aos.model.Subscription;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface SubscriptionRepository extends MongoRepository<Subscription, String> {
    Page<Subscription> findByUser(String user, PageRequest pageRequest);
}
