package gal.usc.etse.aos.repository;

import gal.usc.etse.aos.model.Comment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface CommentRepository extends MongoRepository<Comment, String> {
    Page<Comment> findByPublicationId(String publicationId, PageRequest pageRequest);
}
