package gal.usc.etse.aos.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.access.hierarchicalroles.RoleHierarchy;
import org.springframework.security.access.hierarchicalroles.RoleHierarchyImpl;

@Configuration
public class RolesConfig {
    @Bean
    public RoleHierarchy roleHierarchy() {
        RoleHierarchyImpl hierarchy = new RoleHierarchyImpl();
        hierarchy.setHierarchy("ROLE_ADMIN > ROLE_EDITOR > ROLE_MODERATOR > ROLE_READER");
        //hierarchy.setHierarchy("ROLE_EDITOR > ROLE_MODERATOR");
        //hierarchy.setHierarchy("ROLE_MODERATOR > ROLE_READER");

        return hierarchy;
    }
}
