package gal.usc.etse.aos.controller;

import com.github.fge.jsonpatch.JsonPatch;
import gal.usc.etse.aos.model.Comment;
import gal.usc.etse.aos.model.Subscription;
import gal.usc.etse.aos.repository.CommentRepository;
import gal.usc.etse.aos.repository.SubscriptionRepository;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.RelProvider;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.time.Instant;

@RestController
@RequestMapping("subscriptions")
@Api(
        tags = "Subscription controller",
        description = "Administrate the subscriptions of the app",
        produces = "application/json, application/xml",
        consumes = "application/json, application/xml"
)
public class SubscriptionsController {
    private SubscriptionRepository db;
    private RelProvider relProvider;

    @Autowired
    public SubscriptionsController(SubscriptionRepository db, RelProvider relProvider) {
        this.db = db;
        this.relProvider = relProvider;
    }

    @ApiOperation("Get a subscription")
    @ApiResponses({
            @ApiResponse(code=200, message = "The coment", response = Subscription.class, responseHeaders = {
                    @ResponseHeader(name = "Link", description = "Related links", response = Link.class)
            }),
            @ApiResponse(code=404, message = "Subscription not found")
    })
    @PreAuthorize("hasRole('READER')")
    @GetMapping(
            path = "/{subscription}",
            produces = { MediaType.APPLICATION_JSON_UTF8_VALUE, MediaType.APPLICATION_XML_VALUE }
    )

    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Subscription> getSubscription(@PathVariable("subscription") String subscriptionId){
        if(db.existsById(subscriptionId)) {
            Link self = ControllerLinkBuilder.linkTo(SubscriptionsController.class).slash(subscriptionId).withSelfRel();
            Link all = ControllerLinkBuilder.linkTo(SubscriptionsController.class).withRel(relProvider.getCollectionResourceRelFor(Subscription.class));
            return ResponseEntity
                    .ok()
                    .header(HttpHeaders.LINK, self.toString())
                    .header(HttpHeaders.LINK, all.toString())
                    .body(db.findById(subscriptionId).get());
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @ApiOperation("Get all coments")
    @ApiResponses({
            @ApiResponse(code=200, message = "The list of coments", response = Subscription.class, responseContainer = "List", responseHeaders = {
                    @ResponseHeader(name = "Link", description = "Related links", response = Link.class)
            })
    })
    @PreAuthorize("hasRole('READER')")
    @GetMapping(
            produces = { MediaType.APPLICATION_JSON_UTF8_VALUE, MediaType.APPLICATION_XML_VALUE }
    )
    @ResponseStatus(HttpStatus.OK)
    public  ResponseEntity<Page<Subscription>> getAllSubscriptions(@RequestParam(value = "user", defaultValue = "any") String user, @RequestParam(value = "page", defaultValue = "0") int page, @RequestParam(value = "size", defaultValue = "5") int size, @RequestParam(value = "sort", defaultValue = "author") String sort){

        Sort order = null;
        order = Sort.by(Sort.Order.asc(sort));

        Page<Subscription> subscriptions;
        if(!user.equals("any")){
            subscriptions = db.findByUser(user, PageRequest.of(page, size, order));
        }
        else{
            subscriptions = db.findAll(PageRequest.of(page, size));
        }
        ResponseEntity.BodyBuilder response = ResponseEntity.ok();

        Link self = ControllerLinkBuilder.linkTo(ControllerLinkBuilder.methodOn(SubscriptionsController.class).getAllSubscriptions(user, page, size, sort)).withSelfRel();
        response.header(HttpHeaders.LINK, self.toString());

        if(!subscriptions.isFirst()) {
            Link first = ControllerLinkBuilder
                    .linkTo(ControllerLinkBuilder.methodOn(SubscriptionsController.class).getAllSubscriptions(user, 0, size, sort))
                    .withRel(Link.REL_FIRST).expand();
            Link prev = ControllerLinkBuilder
                    .linkTo(ControllerLinkBuilder.methodOn(SubscriptionsController.class).getAllSubscriptions(user, page - 1, size, sort))
                    .withRel(Link.REL_PREVIOUS);
            response.header(HttpHeaders.LINK, first.toString())
                    .header(HttpHeaders.LINK, prev.toString());
        }

        if(!subscriptions.isLast()){
            Link last = ControllerLinkBuilder
                    .linkTo(ControllerLinkBuilder.methodOn(SubscriptionsController.class).getAllSubscriptions(user, subscriptions.getTotalPages() - 1, size, sort))
                    .withRel(Link.REL_LAST);
            Link next = ControllerLinkBuilder
                    .linkTo(ControllerLinkBuilder.methodOn(SubscriptionsController.class).getAllSubscriptions(user, page + 1, size, sort))
                    .withRel(Link.REL_NEXT);
            response.header(HttpHeaders.LINK, last.toString())
                    .header(HttpHeaders.LINK, next.toString());
        }

        return response.body(subscriptions);
    }

    @ApiOperation("Delete a subscription")
    @ApiResponses({
            @ApiResponse(code=204, message = "Subscription deleted correctly", response = Subscription.class, responseHeaders = {
                    @ResponseHeader(name = "Link", description = "Related links", response = Link.class)
            }),
            @ApiResponse(code=401, message = "Only logged users can access this resource"),
            @ApiResponse(code=403, message = "Only users with role ADMIN can access this resource"),
            @ApiResponse(code=404, message = "User not found")
    })
    @PreAuthorize("hasRole('READER')")
    @DeleteMapping(path = "/{subscription}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity delete(@PathVariable("subscription") String subscription){
        Link all = ControllerLinkBuilder.linkTo(SubscriptionsController.class).withRel(relProvider.getCollectionResourceRelFor(Subscription.class));

        if(!db.existsById(subscription)) {
            return ResponseEntity
                    .notFound()
                    .header(HttpHeaders.LINK, all.toString())
                    .build();
        } else {
            db.deleteById(subscription);
            return ResponseEntity
                    .noContent()
                    .header(HttpHeaders.LINK, all.toString())
                    .build();
        }
    }

    @ApiOperation("Create a subscription")
    @ApiResponses({
            @ApiResponse(code=409, message = "Subscription already exists"),
            @ApiResponse(code=201, message = "Subscription created correctly", response = Subscription.class, responseHeaders = {
                    @ResponseHeader(name = "Location", description = "The location where the created subscription can be found", response = URI.class),
                    @ResponseHeader(name = "Link", description = "Related links", response = Link.class, responseContainer = "List")
            })
    })
    @PreAuthorize("hasRole('READER')")
    @PostMapping(
            produces = { MediaType.APPLICATION_JSON_UTF8_VALUE, MediaType.APPLICATION_XML_VALUE },
            consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE, MediaType.APPLICATION_XML_VALUE }
    )
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Subscription> create(@RequestBody Subscription subscription) {
        Subscription newSubscritpion = new Subscription();
        newSubscritpion.setUser(SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString());
        newSubscritpion.setAuthor(subscription.getAuthor());
        newSubscritpion.setId(newSubscritpion.getUser() + newSubscritpion.getAuthor());
        if(db.existsById(newSubscritpion.getId()))
            return ResponseEntity.status(HttpStatus.CONFLICT).build();
        else{

            db.save(newSubscritpion);

            Link self = ControllerLinkBuilder.linkTo(SubscriptionsController.class).slash(subscription.getId()).withSelfRel();
            Link all = ControllerLinkBuilder.linkTo(SubscriptionsController.class).withRel(relProvider.getCollectionResourceRelFor(Subscription.class));
            return ResponseEntity
                    .created(URI.create(self.getHref()))
                    .header(HttpHeaders.LINK, self.toString())
                    .header(HttpHeaders.LINK, all.toString())
                    .body(newSubscritpion);
        }
    }

    @PreAuthorize("hasRole('READER')")
    @PatchMapping(
            path = "/{subscription}",
            produces = { MediaType.APPLICATION_JSON_UTF8_VALUE, MediaType.APPLICATION_XML_VALUE }
    )
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Subscription> update(@PathVariable("subscriptionId") String subscriptionId, @RequestBody JsonPatch patchOperations) {
        Subscription subscription = db.findById(subscriptionId).get().update(patchOperations);

        db.save(subscription);

        return ResponseEntity.ok(subscription);
    }
}
