package gal.usc.etse.aos.controller;

import com.github.fge.jsonpatch.JsonPatch;
import gal.usc.etse.aos.model.Comment;
import gal.usc.etse.aos.repository.CommentRepository;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.RelProvider;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.time.Instant;

@RestController
@RequestMapping("comments")
@Api(
        tags = "Coments controller",
        description = "Administrate the coments of the app",
        produces = "application/json, application/xml",
        consumes = "application/json, application/xml"
)
public class CommentsController {
    private CommentRepository db;
    private RelProvider relProvider;

    @Autowired
    public CommentsController(CommentRepository db, RelProvider relProvider) {
        this.db = db;
        this.relProvider = relProvider;
    }

    @ApiOperation("Get a comment")
    @ApiResponses({
            @ApiResponse(code=200, message = "The coment", response = Comment.class, responseHeaders = {
                    @ResponseHeader(name = "Link", description = "Related links", response = Link.class)
            }),
            @ApiResponse(code=404, message = "Comment not found")
    })
    @GetMapping(
            path = "/{comment}",
            produces = { MediaType.APPLICATION_JSON_UTF8_VALUE, MediaType.APPLICATION_XML_VALUE }
    )

    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Comment> getComment(@PathVariable("comment") String commentId){
        if(db.existsById(commentId)) {
            Link self = ControllerLinkBuilder.linkTo(CommentsController.class).slash(commentId).withSelfRel();
            Link all = ControllerLinkBuilder.linkTo(CommentsController.class).withRel(relProvider.getCollectionResourceRelFor(Comment.class));
            return ResponseEntity
                    .ok()
                    .header(HttpHeaders.LINK, self.toString())
                    .header(HttpHeaders.LINK, all.toString())
                    .body(db.findById(commentId).get());
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @ApiOperation("Get all coments")
    @ApiResponses({
            @ApiResponse(code=200, message = "The list of coments", response = Comment.class, responseContainer = "List", responseHeaders = {
                    @ResponseHeader(name = "Link", description = "Related links", response = Link.class)
            })
    })
    @PreAuthorize("permitAll()")
    @GetMapping(
            produces = { MediaType.APPLICATION_JSON_UTF8_VALUE, MediaType.APPLICATION_XML_VALUE }
    )
    @ResponseStatus(HttpStatus.OK)
    public  ResponseEntity<Page<Comment>> getAllComments(@RequestParam(value = "publicationId", defaultValue = "any") String publicationId, @RequestParam(value = "page", defaultValue = "0") int page, @RequestParam(value = "size", defaultValue = "5") int size, @RequestParam(value = "sort", defaultValue = "id") String sort){

        Sort order = null;
        order = Sort.by(Sort.Order.asc(sort));

        Page<Comment> comments;
        if(!publicationId.equals("any")){
            comments = db.findByPublicationId(publicationId, PageRequest.of(page, size, order));
        }
        else{
            comments = db.findAll(PageRequest.of(page, size));
        }
        ResponseEntity.BodyBuilder response = ResponseEntity.ok();

        Link self = ControllerLinkBuilder.linkTo(ControllerLinkBuilder.methodOn(CommentsController.class).getAllComments(publicationId, page, size, sort)).withSelfRel();
        response.header(HttpHeaders.LINK, self.toString());

        if(!comments.isFirst()) {
            Link first = ControllerLinkBuilder
                    .linkTo(ControllerLinkBuilder.methodOn(CommentsController.class).getAllComments(publicationId, 0, size, sort))
                    .withRel(Link.REL_FIRST).expand();
            Link prev = ControllerLinkBuilder
                    .linkTo(ControllerLinkBuilder.methodOn(CommentsController.class).getAllComments(publicationId, page - 1, size, sort))
                    .withRel(Link.REL_PREVIOUS);
            response.header(HttpHeaders.LINK, first.toString())
                    .header(HttpHeaders.LINK, prev.toString());
        }

        if(!comments.isLast()){
            Link last = ControllerLinkBuilder
                    .linkTo(ControllerLinkBuilder.methodOn(CommentsController.class).getAllComments(publicationId, comments.getTotalPages() - 1, size, sort))
                    .withRel(Link.REL_LAST);
            Link next = ControllerLinkBuilder
                    .linkTo(ControllerLinkBuilder.methodOn(CommentsController.class).getAllComments(publicationId, page + 1, size, sort))
                    .withRel(Link.REL_NEXT);
            response.header(HttpHeaders.LINK, last.toString())
                    .header(HttpHeaders.LINK, next.toString());
        }

        return response.body(comments);
    }

    @ApiOperation("Delete a comment")
    @ApiResponses({
            @ApiResponse(code=204, message = "Comment deleted correctly", response = Comment.class, responseHeaders = {
                    @ResponseHeader(name = "Link", description = "Related links", response = Link.class)
            }),
            @ApiResponse(code=401, message = "Only logged users can access this resource"),
            @ApiResponse(code=403, message = "Only users with role ADMIN can access this resource"),
            @ApiResponse(code=404, message = "User not found")
    })
    @PreAuthorize("permitAll()")
    @DeleteMapping(path = "/{comment}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity delete(@PathVariable("comment") String comment){
        Link all = ControllerLinkBuilder.linkTo(CommentsController.class).withRel(relProvider.getCollectionResourceRelFor(Comment.class));

        if(!db.existsById(comment)) {
            return ResponseEntity
                    .notFound()
                    .header(HttpHeaders.LINK, all.toString())
                    .build();
        } else {
            db.deleteById(comment);
            return ResponseEntity
                    .noContent()
                    .header(HttpHeaders.LINK, all.toString())
                    .build();
        }
    }

    @ApiOperation("Create a comment")
    @ApiResponses({
            @ApiResponse(code=409, message = "Comment already exists"),
            @ApiResponse(code=201, message = "Comment created correctly", response = Comment.class, responseHeaders = {
                    @ResponseHeader(name = "Location", description = "The location where the created comment can be found", response = URI.class),
                    @ResponseHeader(name = "Link", description = "Related links", response = Link.class, responseContainer = "List")
            })
    })
    @PreAuthorize("permitAll()")
    @PostMapping(
            produces = { MediaType.APPLICATION_JSON_UTF8_VALUE, MediaType.APPLICATION_XML_VALUE },
            consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE, MediaType.APPLICATION_XML_VALUE }
    )
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Comment> create(@RequestBody Comment comment) {
        Comment newComment = new Comment();
        Instant time = Instant.now();
        newComment.setAuthor(SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString());
        newComment.setId(newComment.getAuthor() + time.toString());
        if(db.existsById(newComment.getId()))
            return ResponseEntity.status(HttpStatus.CONFLICT).build();
        else{
            newComment.setBody(comment.getBody());
            newComment.setDate(time);
            newComment.setPublicationId(comment.getPublicationId());


            db.save(newComment);

            Link self = ControllerLinkBuilder.linkTo(CommentsController.class).slash(comment.getId()).withSelfRel();
            Link all = ControllerLinkBuilder.linkTo(CommentsController.class).withRel(relProvider.getCollectionResourceRelFor(Comment.class));
            return ResponseEntity
                    .created(URI.create(self.getHref()))
                    .header(HttpHeaders.LINK, self.toString())
                    .header(HttpHeaders.LINK, all.toString())
                    .body(newComment);
        }
    }

    @PreAuthorize("permitAll()")
    @PatchMapping(
            path = "/{comment}",
            produces = { MediaType.APPLICATION_JSON_UTF8_VALUE, MediaType.APPLICATION_XML_VALUE }
    )
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Comment> update(@PathVariable("commentId") String commentId, @RequestBody JsonPatch patchOperations) {
        Comment comment = db.findById(commentId).get().update(patchOperations);

        db.save(comment);

        return ResponseEntity.ok(comment);
    }
}
