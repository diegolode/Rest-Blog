package gal.usc.etse.aos.controller;

import com.github.fge.jsonpatch.JsonPatch;
import gal.usc.etse.aos.model.FullUser;
import gal.usc.etse.aos.model.auth.Credentials;
import gal.usc.etse.aos.repository.FullUserRepository;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.RelProvider;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("users")
@Api(
        tags = "Users controller",
        description = "Administrate the users of the app",
        produces = "application/json, application/xml",
        consumes = "application/json, application/xml"
)
public class UsersController {
    private FullUserRepository db;
    private PasswordEncoder passwordEncoder;
    private RelProvider relProvider;

    @Autowired
    public UsersController(FullUserRepository db, PasswordEncoder passwordEncoder, RelProvider relProvider) {
        this.passwordEncoder = passwordEncoder;
        this.db = db;
        this.relProvider = relProvider;
    }

    @ApiOperation("Get a user")
    @ApiResponses({
            @ApiResponse(code=200, message = "The user", response = FullUser.class, responseHeaders = {
                    @ResponseHeader(name = "Link", description = "Related links", response = Link.class)
            }),
            @ApiResponse(code=401, message = "Only logged users can access this resource"),
            @ApiResponse(code=403, message = "Only users with role ADMIN can access this resource"),
            @ApiResponse(code=404, message = "User not found")
    })
    @GetMapping(
            path = "/{username}",
            produces = { MediaType.APPLICATION_JSON_UTF8_VALUE, MediaType.APPLICATION_XML_VALUE }
    )
    @PreAuthorize("permitAll()")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<FullUser> getUser(@PathVariable("username") String username){
        if(db.existsById(username)) {
            Link self = ControllerLinkBuilder.linkTo(UsersController.class).slash(username).withSelfRel();
            Link all = ControllerLinkBuilder.linkTo(UsersController.class).withRel(relProvider.getCollectionResourceRelFor(FullUser.class));
            return ResponseEntity
                    .ok()
                    .header(HttpHeaders.LINK, self.toString())
                    .header(HttpHeaders.LINK, all.toString())
                    .body(db.findById(username).get().setPassword(null));
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @ApiOperation("Get all users")
    @ApiResponses({
            @ApiResponse(code=200, message = "The list of users", response = FullUser.class, responseContainer = "List", responseHeaders = {
                    @ResponseHeader(name = "Link", description = "Related links", response = Link.class)
            })
    })
    @PreAuthorize("permitAll()")
    @GetMapping(
            produces = { MediaType.APPLICATION_JSON_UTF8_VALUE, MediaType.APPLICATION_XML_VALUE }
    )
    @ResponseStatus(HttpStatus.OK)
    public  ResponseEntity<Page<FullUser>> getAllUsers(@RequestParam(value = "role", defaultValue = "any") String role, @RequestParam(value = "page", defaultValue = "0") int page, @RequestParam(value = "size", defaultValue = "10") int size,  @RequestParam(value = "sort", defaultValue = "username") String sort){

        Sort order = null;
        order = Sort.by(Sort.Order.asc(sort));

        Page<FullUser> users;
        if(!role.equals("any")){
            users = db.findByRole(role, PageRequest.of(page, size)).map(user -> user.setPassword(null));
        }
        else{
            users = db.findAll(PageRequest.of(page, size)).map(user -> user.setPassword(null));
        }
        ResponseEntity.BodyBuilder response = ResponseEntity.ok();

        Link self = ControllerLinkBuilder.linkTo(ControllerLinkBuilder.methodOn(UsersController.class).getAllUsers(role, page, size, sort)).withSelfRel();
        response.header(HttpHeaders.LINK, self.toString());

        if(!users.isFirst()) {
            Link first = ControllerLinkBuilder
                    .linkTo(ControllerLinkBuilder.methodOn(UsersController.class).getAllUsers(role, 0, size, sort))
                    .withRel(Link.REL_FIRST).expand();
            Link prev = ControllerLinkBuilder
                    .linkTo(ControllerLinkBuilder.methodOn(UsersController.class).getAllUsers(role, page - 1, size, sort))
                    .withRel(Link.REL_PREVIOUS);
            response.header(HttpHeaders.LINK, first.toString())
                    .header(HttpHeaders.LINK, prev.toString());
        }

        if(!users.isLast()){
            Link last = ControllerLinkBuilder
                    .linkTo(ControllerLinkBuilder.methodOn(UsersController.class).getAllUsers(role, users.getTotalPages() - 1, size, sort))
                    .withRel(Link.REL_LAST);
            Link next = ControllerLinkBuilder
                    .linkTo(ControllerLinkBuilder.methodOn(UsersController.class).getAllUsers(role, page + 1, size, sort))
                    .withRel(Link.REL_NEXT);
            response.header(HttpHeaders.LINK, last.toString())
                    .header(HttpHeaders.LINK, next.toString());
        }

        return response.body(users);
    }

    @ApiOperation("Delete a user")
    @ApiResponses({
            @ApiResponse(code=204, message = "User deleted correctly", response = FullUser.class, responseHeaders = {
                    @ResponseHeader(name = "Link", description = "Related links", response = Link.class)
            }),
            @ApiResponse(code=401, message = "Only logged users can access this resource"),
            @ApiResponse(code=403, message = "Only users with role ADMIN can access this resource"),
            @ApiResponse(code=404, message = "User not found")
    })
    @PreAuthorize("permitAll()")
    @DeleteMapping(path = "/{username}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity delete(@PathVariable("username") String username){
        Link all = ControllerLinkBuilder.linkTo(UsersController.class).withRel(relProvider.getCollectionResourceRelFor(FullUser.class));

        if(!db.existsById(username)) {
            return ResponseEntity
                    .notFound()
                    .header(HttpHeaders.LINK, all.toString())
                    .build();
        } else {
            db.deleteById(username);
            return ResponseEntity
                    .noContent()
                    .header(HttpHeaders.LINK, all.toString())
                    .build();
        }
    }

    @ApiOperation("Create a user")
    @ApiResponses({
            @ApiResponse(code=409, message = "User already exists"),
            @ApiResponse(code=201, message = "User created correctly", response = FullUser.class, responseHeaders = {
                    @ResponseHeader(name = "Location", description = "The location where the created user can be found", response = URI.class),
                    @ResponseHeader(name = "Link", description = "Related links", response = Link.class, responseContainer = "List")
            })
    })
    @PreAuthorize("permitAll()")
    @PostMapping(
            produces = { MediaType.APPLICATION_JSON_UTF8_VALUE, MediaType.APPLICATION_XML_VALUE },
            consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE, MediaType.APPLICATION_XML_VALUE }
    )
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<FullUser> create(@RequestBody Credentials credentials) {
        if(db.existsById(credentials.getUsername()))
            return ResponseEntity.status(HttpStatus.CONFLICT).build();
        else{
            String role = "ROLE_READER";

            FullUser user = new FullUser();
            user.setUsername(credentials.getUsername());
            user.setPassword(passwordEncoder.encode(credentials.getPassword()));
            user.setRegisterDate(Instant.now());
            user.setFullname(credentials.getFullname());
            user.setEmail(credentials.getEmail());
            user.setAvatar(credentials.getAvatar());
            user.setRole(role);
            user.setEnabled(true);

            db.save(user);

            Link self = ControllerLinkBuilder.linkTo(UsersController.class).slash(user.getUsername()).withSelfRel();
            Link all = ControllerLinkBuilder.linkTo(UsersController.class).withRel(relProvider.getCollectionResourceRelFor(FullUser.class));
            return ResponseEntity
                    .created(URI.create(self.getHref()))
                    .header(HttpHeaders.LINK, self.toString())
                    .header(HttpHeaders.LINK, all.toString())
                    .body(user.setPassword(null));
        }
    }

    @PreAuthorize("permitAll()")
    @PatchMapping(
            path = "/{username}",
            produces = { MediaType.APPLICATION_JSON_UTF8_VALUE, MediaType.APPLICATION_XML_VALUE }
    )
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<FullUser> update(@PathVariable("username") String username, @RequestBody JsonPatch patchOperations) {
        FullUser user = db.findById(username).get().update(patchOperations);

        db.save(user);

        return ResponseEntity.ok(user);
    }
}
