package gal.usc.etse.aos.controller;

import com.github.fge.jsonpatch.JsonPatch;
import gal.usc.etse.aos.filter.AuthorizationFilter;
import gal.usc.etse.aos.model.FullUser;
import gal.usc.etse.aos.model.Publication;
import gal.usc.etse.aos.model.auth.Credentials;
import gal.usc.etse.aos.repository.PublicationRepository;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.RelProvider;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("publications")
@Api(
        tags = "Publications controller",
        description = "Administrate the publications of the app",
        produces = "application/json, application/xml",
        consumes = "application/json, application/xml"
)
public class PublicationsController {
    private PublicationRepository db;
    private RelProvider relProvider;

    @Autowired
    public PublicationsController(PublicationRepository db, RelProvider relProvider) {
        this.db = db;
        this.relProvider = relProvider;
    }

    @ApiOperation("Get a publication")
    @ApiResponses({
            @ApiResponse(code=200, message = "The publication", response = Publication.class, responseHeaders = {
                    @ResponseHeader(name = "Link", description = "Related links", response = Link.class)
            }),
            @ApiResponse(code=404, message = "Publication not found")
    })
    @GetMapping(
            path = "/{publication}",
            produces = { MediaType.APPLICATION_JSON_UTF8_VALUE, MediaType.APPLICATION_XML_VALUE }
    )

    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Publication> getPublication(@PathVariable("publication") String publicationId){
        if(db.existsById(publicationId)) {
            Link self = ControllerLinkBuilder.linkTo(PublicationsController.class).slash(publicationId).withSelfRel();
            Link all = ControllerLinkBuilder.linkTo(PublicationsController.class).withRel(relProvider.getCollectionResourceRelFor(Publication.class));
            return ResponseEntity
                    .ok()
                    .header(HttpHeaders.LINK, self.toString())
                    .header(HttpHeaders.LINK, all.toString())
                    .body(db.findById(publicationId).get());
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping(
            path = "/img",
            produces = { MediaType.APPLICATION_JSON_UTF8_VALUE, MediaType.APPLICATION_XML_VALUE }
    )
    public List<String> getImages() throws URISyntaxException {
        File f = Paths.get(this.getClass().getResource("/img/").toURI()).toFile();
        return Arrays.stream(f.listFiles()).map(File::getName).collect(Collectors.toList());
    }

    @ApiOperation("Get all publications")
    @ApiResponses({
            @ApiResponse(code=200, message = "The list of publications", response = Publication.class, responseContainer = "List", responseHeaders = {
                    @ResponseHeader(name = "Link", description = "Related links", response = Link.class)
            })
    })
    @PreAuthorize("permitAll()")
    @GetMapping(
            produces = { MediaType.APPLICATION_JSON_UTF8_VALUE, MediaType.APPLICATION_XML_VALUE }
    )
    @ResponseStatus(HttpStatus.OK)
    public  ResponseEntity<Page<Publication>> getAllPublications(@RequestParam(value = "author", defaultValue = "any") String author, @RequestParam(value = "page", defaultValue = "0") int page, @RequestParam(value = "size", defaultValue = "2") int size, @RequestParam(value = "sort", defaultValue = "date") String sort){

        Sort order = null;
        order = Sort.by(Sort.Order.asc(sort));

        Page<Publication> publications;
        if(!author.equals("any")){
            publications = db.findByAuthor(author, PageRequest.of(page, size, order));
        }
        else{
            publications = db.findAll(PageRequest.of(page, size));
        }
        ResponseEntity.BodyBuilder response = ResponseEntity.ok();

        Link self = ControllerLinkBuilder.linkTo(ControllerLinkBuilder.methodOn(PublicationsController.class).getAllPublications(author, page, size, sort)).withSelfRel();
        response.header(HttpHeaders.LINK, self.toString());

        if(!publications.isFirst()) {
            Link first = ControllerLinkBuilder
                    .linkTo(ControllerLinkBuilder.methodOn(PublicationsController.class).getAllPublications(author, 0, size, sort))
                    .withRel(Link.REL_FIRST).expand();
            Link prev = ControllerLinkBuilder
                    .linkTo(ControllerLinkBuilder.methodOn(PublicationsController.class).getAllPublications(author, page - 1, size, sort))
                    .withRel(Link.REL_PREVIOUS);
            response.header(HttpHeaders.LINK, first.toString())
                    .header(HttpHeaders.LINK, prev.toString());
        }

        if(!publications.isLast()){
            Link last = ControllerLinkBuilder
                    .linkTo(ControllerLinkBuilder.methodOn(PublicationsController.class).getAllPublications(author, publications.getTotalPages() - 1, size, sort))
                    .withRel(Link.REL_LAST);
            Link next = ControllerLinkBuilder
                    .linkTo(ControllerLinkBuilder.methodOn(PublicationsController.class).getAllPublications(author, page + 1, size, sort))
                    .withRel(Link.REL_NEXT);
            response.header(HttpHeaders.LINK, last.toString())
                    .header(HttpHeaders.LINK, next.toString());
        }

        return response.body(publications);
    }

    @ApiOperation("Delete a publication")
    @ApiResponses({
            @ApiResponse(code=204, message = "Publication deleted correctly", response = Publication.class, responseHeaders = {
                    @ResponseHeader(name = "Link", description = "Related links", response = Link.class)
            }),
            @ApiResponse(code=401, message = "Only logged users can access this resource"),
            @ApiResponse(code=403, message = "Only users with role ADMIN can access this resource"),
            @ApiResponse(code=404, message = "User not found")
    })
    @PreAuthorize("hasRole('EDITOR')")
    @DeleteMapping(path = "/{publication}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity delete(@PathVariable("publication") String publication){
        Optional<Publication> pub = db.findById(publication);
        Link all = ControllerLinkBuilder.linkTo(PublicationsController.class).withRel(relProvider.getCollectionResourceRelFor(Publication.class));

        if(pub.isPresent() && ((pub.get().getAuthor() == SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString())
                || SecurityContextHolder.getContext().getAuthentication().getAuthorities().contains(new SimpleGrantedAuthority(("ROLE_ADMIN"))))){
            db.deleteById(publication);
            return ResponseEntity
                    .noContent()
                    .header(HttpHeaders.LINK, all.toString())
                    .build();
        } else if(!pub.isPresent()){
            return ResponseEntity
                    .notFound()
                    .header(HttpHeaders.LINK, all.toString())
                    .build();
        } else {
            return null;
        }
    }


    @ApiOperation("Create a publication")
    @ApiResponses({
            @ApiResponse(code=409, message = "Publication already exists"),
            @ApiResponse(code=201, message = "Publication created correctly", response = Publication.class, responseHeaders = {
                    @ResponseHeader(name = "Location", description = "The location where the created publication can be found", response = URI.class),
                    @ResponseHeader(name = "Link", description = "Related links", response = Link.class, responseContainer = "List")
            })
    })
    @PreAuthorize("hasRole('EDITOR')")
    @PostMapping(
            produces = { MediaType.APPLICATION_JSON_UTF8_VALUE, MediaType.APPLICATION_XML_VALUE },
            consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE, MediaType.APPLICATION_XML_VALUE }
    )
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Publication> create(@RequestBody Publication publication) {
        Publication newPublication = new Publication();
        newPublication.setAuthor(SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString());
        Instant time = Instant.now();
        newPublication.setId(newPublication.getAuthor() + time.toString());
        if(db.existsById(newPublication.getId()))
            return ResponseEntity.status(HttpStatus.CONFLICT).build();
        else{
            newPublication.setTitle(publication.getTitle());
            newPublication.setBody(publication.getBody());
            newPublication.setDate(time);
            newPublication.setSummary(publication.getSummary());
            newPublication.setKeywords(publication.getKeywords());
            newPublication.setCard1(publication.getCard1());
            newPublication.setCard2(publication.getCard2());
            newPublication.setCard3(publication.getCard3());
            newPublication.setCard4(publication.getCard4());
            newPublication.setCard5(publication.getCard5());
            newPublication.setCard6(publication.getCard6());
            newPublication.setCard7(publication.getCard7());
            newPublication.setCard8(publication.getCard8());


            db.save(newPublication);

            Link self = ControllerLinkBuilder.linkTo(PublicationsController.class).slash(publication.getId()).withSelfRel();
            Link all = ControllerLinkBuilder.linkTo(PublicationsController.class).withRel(relProvider.getCollectionResourceRelFor(Publication.class));
            return ResponseEntity
                    .created(URI.create(self.getHref()))
                    .header(HttpHeaders.LINK, self.toString())
                    .header(HttpHeaders.LINK, all.toString())
                    .body(newPublication);
        }
    }

    @PreAuthorize("permitAll()")
    @PutMapping(
            path = "/{publication}",
            produces = { MediaType.APPLICATION_JSON_UTF8_VALUE, MediaType.APPLICATION_XML_VALUE }
    )
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Publication> update(@PathVariable("publication") String publicationId, @RequestBody Publication publication) {

        db.save(publication);

        return ResponseEntity.ok(publication);
    }
}
