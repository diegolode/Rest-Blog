package gal.usc.etse.aos.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.github.fge.jackson.JacksonUtils;
import com.github.fge.jsonpatch.JsonPatch;
import com.github.fge.jsonpatch.JsonPatchException;
import gal.usc.etse.aos.model.auth.Credentials;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@ApiModel(value = "App user", description = "An user in the app", parent = Credentials.class)
@JsonInclude(JsonInclude.Include.NON_NULL)
@Document(collection = "users")
public class FullUser extends Credentials {
    @ApiModelProperty(name = "role", value = "User role", allowableValues = "ROLE_ADMIN > ROLE_EDITOR > ROLE_MODERATOR > ROLE_READER", example = "ADMIN", required = true)
    private String role;

    public String getRole() {
        return role;
    }

    public FullUser setRole(String role) {
        this.role = role;
        return this;
    }

    @ApiModelProperty(name = "enabled", value = "User banned or not", allowableValues = "true, false", example = "true", required = true)
    private boolean enabled;

    public boolean getEnabled() {
        return enabled;
    }

    public FullUser setEnabled(boolean enabled) {
        this.enabled = enabled;
        return this;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        FullUser fullUser = (FullUser) o;

        return role != null ? role.equals(fullUser.role) : fullUser.role == null;
    }

    @Override
    public FullUser setPassword(String password) {
        super.setPassword(password);
        return this;
    }

    @Override
    public FullUser setUsername(String username) {
        super.setUsername(username);
        return this;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (role != null ? role.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return String.format("FullUser{ username='%s', password='%s', role='%s'}", super.getUsername(), super.getPassword(), role);
    }

    public FullUser update(JsonPatch patch){
        try {
            ObjectMapper mapper = JacksonUtils.newMapper();
            mapper.registerModule(new JavaTimeModule());
            return mapper.convertValue(patch.apply(mapper.convertValue(this, JsonNode.class)), FullUser.class);
        } catch (JsonPatchException e) {
            e.printStackTrace();
        }

        return null;
    }
}
