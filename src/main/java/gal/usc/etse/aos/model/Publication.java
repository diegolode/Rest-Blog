package gal.usc.etse.aos.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jackson.JacksonUtils;
import com.github.fge.jsonpatch.JsonPatch;
import com.github.fge.jsonpatch.JsonPatchException;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.Instant;
import java.util.List;

@ApiModel(value = "App publication", description = "A publication in the app")
@JsonInclude(JsonInclude.Include.NON_NULL)
@Document(collection = "publications")
public class Publication {

    @ApiModelProperty(name = "id", value = "The publication id", example = "diegolode2018-11-20t10-49-24-103z", required = true)
    @Id
    private String id;

    @ApiModelProperty(name = "title", value = "The publication title", example = "New logbait deck", required = true)
    private String title;

    @ApiModelProperty(name = "body", value = "The publication body", example = "This are the cards of the new logbait deck...", required = true)
    private String body;

    @ApiModelProperty(name = "author", value = "The publication author", example = "diegolode", required = true)
    private String author;

    @ApiModelProperty(name = "date", value = "The publication date", example = "2018-11-20t10-49-24-103z", required = true)
    private Instant date;

    @ApiModelProperty(name = "summary", value = "The publication summary", example = "The new logbait deck", required = true)
    private String summary;

    @ApiModelProperty(name = "keywords", value = "The publication keywords", example = "decks, strategy", required = true)
    private List<String> keywords;

    @ApiModelProperty(name = "card1", value = "The first card of the deck", example = "../img/hog_rider.png", required = true)
    private String card1;

    @ApiModelProperty(name = "card2", value = "The second card of the deck", example = "../img/hog_rider.png", required = true)
    private String card2;

    @ApiModelProperty(name = "card3", value = "The third card of the deck", example = "../img/hog_rider.png", required = true)
    private String card3;

    @ApiModelProperty(name = "card4", value = "The fourth card of the deck", example = "../img/hog_rider.png", required = true)
    private String card4;

    @ApiModelProperty(name = "card5", value = "The fifth card of the deck", example = "../img/hog_rider.png", required = true)
    private String card5;

    @ApiModelProperty(name = "card6", value = "The sixth card of the deck", example = "../img/hog_rider.png", required = true)
    private String card6;

    @ApiModelProperty(name = "card7", value = "The seventh card of the deck", example = "../img/hog_rider.png", required = true)
    private String card7;

    @ApiModelProperty(name = "card8", value = "The eighth card of the deck", example = "../img/hog_rider.png", required = true)
    private String card8;

    public String getId() {
        return id;
    }

    public Publication setId(String id) {
        this.id = id;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public Publication setTitle(String title) {
        this.title = title;
        return this;
    }

    public String getBody() {
        return body;
    }

    public Publication setBody(String body) {
        this.body = body;
        return this;
    }

    public String getAuthor() {
        return author;
    }

    public Publication setAuthor(String author) {
        this.author = author;
        return this;
    }

    public Instant getDate() {
        return date;
    }

    public Publication setDate(Instant date) {
        this.date = date;
        return this;
    }

    public String getSummary() {
        return summary;
    }

    public Publication setSummary(String summary) {
        this.summary = summary;
        return this;
    }

    public List<String> getKeywords() {
        return keywords;
    }

    public Publication setKeywords(List<String> keywords) {
        this.keywords = keywords;
        return this;
    }

    public String getCard1() {
        return card1;
    }

    public Publication setCard1(String card1) {
        this.card1 = card1;
        return this;
    }

    public String getCard2() {
        return card2;
    }

    public Publication setCard2(String card2) {
        this.card2 = card2;
        return this;
    }

    public String getCard3() {
        return card3;
    }

    public Publication setCard3(String card3) {
        this.card3 = card3;
        return this;
    }

    public String getCard4() {
        return card4;
    }

    public Publication setCard4(String card4) {
        this.card4 = card4;
        return this;
    }

    public String getCard5() {
        return card5;
    }

    public Publication setCard5(String card5) {
        this.card5 = card5;
        return this;
    }

    public String getCard6() {
        return card6;
    }

    public Publication setCard6(String card6) {
        this.card6 = card6;
        return this;
    }

    public String getCard7() {
        return card7;
    }

    public Publication setCard7(String card7) {
        this.card7 = card7;
        return this;
    }

    public String getCard8() {
        return card8;
    }

    public Publication setCard8(String card8) {
        this.card8 = card8;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Publication publication = (Publication) o;

        return id != null ? id.equals(publication.id) : publication.id == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (id != null ? id.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return String.format("Publication{ id='%s', title='%s', body='%s', author='%s', date='%s', summary='%s', keywords=%s, card1='%s', card2='%s', card3='%s', card4='%s', card5='%s', card6='%s', card7='%s', card8='%s'}",
                                               id, title, body, author, date, summary, keywords, card1, card2, card3, card4, card5, card6, card7, card8);
    }

    public Publication update(JsonPatch patch){
        try {
            ObjectMapper mapper = JacksonUtils.newMapper();
            return mapper.convertValue(patch.apply(mapper.convertValue(this, JsonNode.class)), Publication.class);
        } catch (JsonPatchException e) {
            e.printStackTrace();
        }

        return null;
    }
}
