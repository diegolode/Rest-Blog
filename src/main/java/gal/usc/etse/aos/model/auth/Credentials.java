package gal.usc.etse.aos.model.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.data.annotation.Id;

import java.time.Instant;

@ApiModel(value = "Credentials", description = "The user credentials")
public class Credentials {

    @ApiModelProperty(name = "username", value = "The user name", example = "diegolode", required = true)
    @Id
    private String username;

    @ApiModelProperty(name = "password", value = "The user password", example = "dlv", required = true)
    private String password;

    @ApiModelProperty(name = "fullname", value = "The user full name", example = "Diego Lodeiro Vidal")
    private String fullname;

    @ApiModelProperty(name = "registerDate", value = "The register date", example = "01/11/2018", required = true)
    private Instant registerDate;

    @ApiModelProperty(name = "email", value = "The user email", example = "diego@gmail.com", required = true)
    private String email;

    @ApiModelProperty(name = "avatar", value = "The user avatar", example = "http://localhost:8081/img/knight.png", required = true)
    private String avatar;


    public String getUsername() {
        return username;
    }

    public Credentials setUsername(String username) {
        this.username = username;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public Credentials setPassword(String password) {
        this.password = password;
        return this;
    }

    public String getFullname() {
        return fullname;
    }

    public Credentials setFullname(String fullname) {
        this.fullname = fullname;
        return this;
    }

    public Instant getRegisterDate() {
        return registerDate;
    }

    public Credentials setRegisterDate(Instant registerDate) {
        this.registerDate = registerDate;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public Credentials setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getAvatar() {
        return avatar;
    }

    public Credentials setAvatar(String avatar) {
        this.avatar = avatar;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Credentials that = (Credentials) o;

        if (username != null ? !username.equals(that.username) : that.username != null) return false;
        return password != null ? password.equals(that.password) : that.password == null;
    }

    @Override
    public int hashCode() {
        int result = username != null ? username.hashCode() : 0;
        result = 31 * result + (password != null ? password.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return String.format("Credentials{ username='%s', password='%s', fullname='%s', registerDate='%s', email='%s'}",
                                               username, password, fullname, registerDate, email);
    }
}
