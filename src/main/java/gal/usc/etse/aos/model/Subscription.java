package gal.usc.etse.aos.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jackson.JacksonUtils;
import com.github.fge.jsonpatch.JsonPatch;
import com.github.fge.jsonpatch.JsonPatchException;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.Instant;

@ApiModel(value = "App subscription", description = "A user's suscription in the app")
@JsonInclude(JsonInclude.Include.NON_NULL)
@Document(collection = "subscriptions")
public class Subscription {

    @ApiModelProperty(name = "id", value = "The subscription id", example = "diegolode-withzack", required = true)
    @Id
    private String id;

    @ApiModelProperty(name = "user", value = "The user who makes the suscription", example = "diegolode", required = true)
    private String user;

    @ApiModelProperty(name = "author", value = "The author which user is suscribed", example = "withzack", required = true)
    private String author;

    public String getId() {
        return id;
    }

    public Subscription setId(String id) {
        this.id = id;
        return this;
    }

    public String getUser() {
        return user;
    }

    public Subscription setUser(String user) {
        this.user = user;
        return this;
    }

    public String getAuthor() {
        return author;
    }

    public Subscription setAuthor(String author) {
        this.author = author;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Subscription subscription = (Subscription) o;

        return id != null ? id.equals(subscription.id) : subscription.id == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (id != null ? id.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return String.format("Subscription{ id='%s', user='%s', author='%s'}",
                id, user, author);
    }

    public Subscription update(JsonPatch patch){
        try {
            ObjectMapper mapper = JacksonUtils.newMapper();
            return mapper.convertValue(patch.apply(mapper.convertValue(this, JsonNode.class)), Subscription.class);
        } catch (JsonPatchException e) {
            e.printStackTrace();
        }

        return null;
    }
}
