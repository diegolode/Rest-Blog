package gal.usc.etse.aos.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jackson.JacksonUtils;
import com.github.fge.jsonpatch.JsonPatch;
import com.github.fge.jsonpatch.JsonPatchException;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.Instant;

@ApiModel(value = "App coment", description = "A coment in the app")
@JsonInclude(JsonInclude.Include.NON_NULL)
@Document(collection = "comments")
public class Comment {

    @ApiModelProperty(name = "id", value = "The coment id", example = "diegolode2018-11-21t05-34-25-103z", required = true)
    @Id
    private String id;

    @ApiModelProperty(name = "body", value = "The coment body", example = "This are the cards of the new logbait deck...", required = true)
    private String body;

    @ApiModelProperty(name = "author", value = "The coment author", example = "diegolode", required = true)
    private String author;

    @ApiModelProperty(name = "date", value = "The coment date", example = "2018-11-21t05-34-25-103z", required = true)
    private Instant date;

    @ApiModelProperty(name = "publicationId", value = "The publication which is comented", example = "The new logbait deck", required = true)
    private String publicationId;

    public String getId() {
        return id;
    }

    public Comment setId(String id) {
        this.id = id;
        return this;
    }

    public String getBody() {
        return body;
    }

    public Comment setBody(String body) {
        this.body = body;
        return this;
    }

    public String getAuthor() {
        return author;
    }

    public Comment setAuthor(String author) {
        this.author = author;
        return this;
    }

    public Instant getDate() {
        return date;
    }

    public Comment setDate(Instant date) {
        this.date = date;
        return this;
    }

    public String getPublicationId() {
        return publicationId;
    }

    public Comment setPublicationId(String publicationId) {
        this.publicationId = publicationId;
        return this;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Comment comment = (Comment) o;

        return id != null ? id.equals(comment.id) : comment.id == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (id != null ? id.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return String.format("Comment{ id='%s', body='%s', author='%s', date='%s', publicationId='%s'}",
                                               id, body, author, date, publicationId);
    }

    public Comment update(JsonPatch patch){
        try {
            ObjectMapper mapper = JacksonUtils.newMapper();
            return mapper.convertValue(patch.apply(mapper.convertValue(this, JsonNode.class)), Comment.class);
        } catch (JsonPatchException e) {
            e.printStackTrace();
        }

        return null;
    }
}
