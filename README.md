REST app example with SpringBoot
--------------------------------

This is a SpringBoot example application. You can find the code in https://gitlab.citius.usc.es/victorjose.gallego/RESTExample/tags.
This example is incremental, each increment is tagged in this repository:

- `basic`: basic example whit code to create simple REST endpoits
- `with-db`: the same example as before, but adding a connection to a mongo database
- `with-auth`: the same example as before, but adding JWT authentication
- `with-hidden-fields`: the same as before, but configuring Jackson to hide the null fields
- `with-swagger`: the same as before, but with documentation examples in Swagger
- `with-hateoas`: the same as before, but with HATEOAS links included

You can see the changes between examples comparing the tags in this repo. 
That allows you to see which code you have to add to implement the changes.